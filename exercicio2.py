#Exercício 1

print('Entre com dois números inteiros:')
numero1 = int(input('1º número: '))
numero2 = int(input('2º número: '))

for i in range(numero1,numero2+1):
    print(i)

# ---------------------------------------------

#Exercício 2

print('Entre com um número inteiro entre 1 e 10:')
numero = int(input('número: '))

for i in range(1,11):
    print(f'{i} x {numero} =  {numero*i}')


