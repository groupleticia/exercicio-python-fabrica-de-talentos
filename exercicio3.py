#Exercício 3

def acao_inserir():
    marca = input('Digite a marca: ')
    modelo = input('Digite o modelo: ')
    preco = float(input('Digite o preço: '))
    indice = int(input('Digite o indice: '))

    lista_carros.append({'marca': marca, 'modelo': modelo, 'preco': preco, 'indice': indice})

    print('Carro adicionado!')

def acao_retirar(indice):
    del(lista_carros[indice])
    print('Carro Removido!')

def acao_listar():
    print(lista_carros)


lista_carros = [
    {'marca': 'Fiat', 'modelo':'modelo1', 'preco': 50000.0, 'indice': 0},
    {'marca': 'Ford', 'modelo':'modelo2', 'preco': 60000.0, 'indice': 1}
]

while True:
    print('1. Listar carros')
    print('2. Comprar')
    print('3. Inserir no catálogo')
    print('4. Sair')


    choice = int(input('Digite a opção: '))

    if choice == 1:
        acao_listar()
    elif choice == 2:
        indice = int(input('Digite o valor do indice que deseja comprar: '))
        acao_retirar(indice)
    elif choice == 3:
        acao_inserir()
    elif choice == 4:
        break
    else:
        print('Opção Inválida!')

