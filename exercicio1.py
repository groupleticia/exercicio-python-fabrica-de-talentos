# Exercício 1

print('Entre com 2 números: ')
numero1 = float(input('Entre com o primeiro número: '))
numero2 = float(input('Entre com o segundo número: '))

if numero1>numero2:
    print(numero1)
else:
    print(numero2)

-----------------------------------------------------------------------------

# Exercício 2

print('Entre com o percentual do índice de produção: ')
percentual = float(input('Digite o percentual: '))

if percentual < 1:
    print('A empresa apresentou decrescimento no índice de produção.')
elif percentual == 1:
    print('A empresa manteve o índice de produção anterior.')
else:
    print('A empresa apresentou crescimento no índice de produção.')

