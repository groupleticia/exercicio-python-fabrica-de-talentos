print('Leticia', 10)

nome = input('Escreva seu nome: ')
print(type(nome))

ano_entrada = int(input('Escreva o ano de ingresso do estudante: '))
print(type(ano_entrada))

nota = float(input('Digite a nota do estudante: '))
print(type(nota))

if 1>2:
    print('Condição Verdadeira')
print('fora da condicao')

media = float(input('Digite a media: '))

if media>=60:
    print('Aprovado')
elif 60 > media >= 40:
    print('NP3')
else:
    print('Reprovado')

t1 = t2 = True
f1 = f2 = False

if t1 and f2:
    print('expressao verdadeira')
else:
    print('expressao falsa')

if t1 or f2:
    print('expressao verdadeira')
else:
    print('expressao falsa')

if not f1:
    print('expressao verdadeira')
else:
    print('expressao falsa')

lista = 'Leandro, Guilherme, João, Pedro, Ana'

nome_1 = 'Leandro'
nome_2 = 'Jose'

if nome_2 in lista:
    print(f'{nome_2} está na lista')
else:
    print(f'{nome_2} não está na lista')

cont = 1

while cont <= 3:
    nota_1 = float(input('Digite a 1ª  nota: '))
    nota_2 = float(input('Digite a 2ª  nota: '))

    print(f'Média: {(nota_1 + nota_2) / 2}')

    cont+=1

for cont in range(1,11):
    print(cont)

for cont in range(1,4):
    nota_1 = float(input('Digite a 1ª  nota: '))
    nota_2 = float(input('Digite a 2ª  nota: '))

    print(f'Média: {(nota_1 + nota_2) / 2}')

lista = ['Fabricio', 9.5, 9.0, 8.0, True]

print(lista)

print(lista[0])

for elemento in lista:
    print(elemento)

lista[3]=10

for elemento in lista:
    print(elemento)

media = (lista[1] + lista[2] + lista[3])/3
print(f'Media: {media}')

print(len(lista))

lista.append(media)
print(lista)

lista.extend([10.0,8.0,9.0])
print(lista)


lista.remove(9.0)
print(lista)


dicionario = {
    'chave1':1,
    'chave2':2
}

print(dicionario)

cadastro = {
    'matricula': 2159753,
    'dia_cadastro':25,
    'mes_cadastro':10,
    'turma': '2E'
}

print(cadastro['matricula'])
print(cadastro['turma'])

cadastro['turma'] = '2G'
print(cadastro['turma'])

cadastro['modalidade'] = 'EAD'
print(cadastro)

cadastro.pop('turma')

print(cadastro.items())
print(cadastro)

print(cadastro.keys())
print(cadastro.values())

for chaves in cadastro.keys():
    print(cadastro[chaves])

for valores in cadastro.values():
    print(valores)

for chaves, valores in cadastro.items():
    print(chaves, valores)


def media():
    nota_1 = float(input('Digite a nota 1: '))
    nota_2 = float(input('Digite a nota 2: '))

    print(f'Média = {(nota_1+nota_2)/2}')

while True:
    print('1. Calcular média')
    print('2. Sair')

    choice = int(input('Digite a opção: '))

    if choice == 1:
        media()
    elif choice == 2:
        break
    else:
        print('Opção Inválida!')

lista = [
    {'aluno': 'Davi', 'nota_1':10, 'nota2': 8},
    {'aluno': 'Leandro', 'nota_1': 7, 'nota2': 9}
]
print(lista)

aux = int(input('Digite quantos alunos possui: '))

for count in range(1,aux+1):
    aluno = input('Digite o nome do aluno: ')
    nota_1 = float(input('Digite a nota 1: '))
    nota_2 = float(input('Digite a nota 2: '))

    lista.append({'aluno':aluno,'nota_1':nota_1,'nota_2':nota_2})

    print('Aluno adicionado!')

print(lista)

